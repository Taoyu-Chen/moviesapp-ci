import React from "react";
import { Link } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  keywordsbtn: {
    marginLeft: 20
  }
}));
const KeyWordButton = ({ movie }) => {
  const classes = useStyles();


  return (
    <Link to={`/keywords/${movie.id}`}
    >
      <Button variant="contained" color="primary"
        data-cy="keywordsButton"

        className={classes.keywordsbtn}
      >
        movies keywords
      </Button>
    </Link>
  );
};

export default KeyWordButton