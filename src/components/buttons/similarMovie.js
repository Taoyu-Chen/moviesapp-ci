import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  similarbtn: {
    marginLeft: 30
  }
}));

const SimilarMovieButton = ({ movie }) => {
  const classes = useStyles();

  return (
    <>
      <Link to={`/similar/${movie.id}`}>
        <Button variant="contained" color="primary"

          className={classes.similarbtn}
          data-cy="similarMovieButton">
          similar movies
        </Button>
      </Link>
    </>
  );
};



export default SimilarMovieButton;