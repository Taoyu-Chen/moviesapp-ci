# Assignment 1 - Agile Software Practice.

Name: Taoyu Chen

## App Features.

+ Top Rated Movies page: Displays the top 20 movies.  And test material ui components could work well.

Tests: cypress/integration/topRated-page.spec.js

![][topratedmovie]

+ Similar Movie page: Displays the 20 similar movies. When you click "similar movies" button in top rated page should show 20 movies more similar to this movie. This page is similar to top rated page.

Tests: cypress/integration/similar-movie-page.spec.js

![][similarmovie]



+ Movie Details With Keywords page - Shows the details and keywords about a movie.  When you click "movies keywords" button in top rated page should show that movie details and keywords. 

Tests: cypress/integration/movieDetailPageWithKeyWords.spec.js

![][movieKeywords]

+ materialuiSiteHeader: Displays the email of user.  The "Signup" button should display signup page. The "home" button should display home page.  The "Upcoming" button should display upcoming page.  The "Favorite Movie" button should display favorite page. The "Top rated" button should display top rated page. The "Logout" button should allow user to log out and display login page

Tests: cypress/integration/materialuiSiteHeader.spec.js

![][siteheader]

![][signup]	

+ Login Page: Tests the login process and private route. When user didn't login, they can't visit top rated page. If type wrong password, it will show the alert. If login successful, user will see top rated page.

Tests: cypress/integration/firebase-login.spec.js

![][login]

## Testing.

Cypress Dashboard URL: https://dashboard.cypress.io/projects/pa3h1v/runs?branches=%5B%5D&committers=%5B%5D&flaky=%5B%5D&page=1&status=%5B%5D&tags=%5B%5D&timeRange=%7B%22startDate%22%3A%221970-01-01%22%2C%22endDate%22%3A%222038-01-19%22%7D

### Advanced Testing (If required).

+ cypress/integration/firebase-login.js - test private route whether works
+ cypress/integration/similar-movie-page.spec.js - test when no results matched and use data-value to get component
+ cypress/integration/topRated.spec.js - test when not log in the website what will happen

## Independent learning (If relevant).

[login]: ./public/login.png
[signup]: ./public/signup.png
[similarmovie]: ./public/SimilarMoviePage.png
[topratedmovie]: ./public/TopRatedMovie.png
[movieKeywords]: ./public/moviekeywords.png
[Siteheader]: ./public/SiteHeader.png

