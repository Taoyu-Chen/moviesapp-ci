import { AttachFileRounded } from "@material-ui/icons";

let movies;    // List of movies from TMDB
const emailString = '20091612@wit.ie';
const passwordString = '123456';

// Utility functions
const filterByTitle = (movieList, string) =>
  movieList.filter((m) => m.title.toLowerCase().search(string) !== -1);

const filterByGenre = (movieList, genreId) =>
  movieList.filter((m) => m.genre_ids.includes(genreId));

describe("Material ui SiteHeader Page ", () => {
  before(() => {
    cy.visit("/");
    cy.get('[data-cy=loginButton]').click();
    cy.get('[data-cy=email]').clear().type(emailString);
    cy.get('[data-cy=password]').clear().type(passwordString);
    cy.get('[data-cy=login]').click();
    cy.url().should("include", `/movies/top_rated`);  
  })
  
  beforeEach(() => {
    cy.visit("/");
    cy.get('[data-cy=toprate_button]').click();
  });

  describe("Material ui siteheader  test", () => {
    it("displays email when page is loaded", () => {
      cy.get('[data-cy=email]').contains(emailString);
    });
    it("displays home page when click home button", () => {
      cy.get('[data-cy=homeButton]').click();
      cy.url().should("not.include", `/top_rated`);
      cy.get("h2").contains("No. Movies");
    });

    it("displays upcoming movie page when click upcoming button", () => {
      cy.get('[data-cy=upcomingButton]').click();
      cy.url().should("not.include", `/top_rated`);
      cy.get("h2").contains("Upcoming Movies");
    });
    it("displays favorite movie page when click favorite button", () => {
      cy.get('[data-cy=favoriteButton]').click();
      cy.url().should("not.include", `/top_rated`);
      cy.get("h2").contains("Favorite Movies");
    });
  });
  describe("Fire base function test", () => {
    it("displays login page when click logout button", () => {
      cy.get('[data-cy=logoutButton]').click();
      cy.url().should("include", `/login`);
    });
  });
})