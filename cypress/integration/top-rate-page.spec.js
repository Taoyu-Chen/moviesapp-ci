let movies;    // List of movies from TMDB
const emailString = '20091612@wit.ie';
const passwordString = '123456';
// Utility functions
const filterByTitle = (movieList, string) =>
  movieList.filter((m) => m.title.toLowerCase().search(string) !== -1);

const filterByGenre = (movieList, genreId) =>
  movieList.filter((m) => m.genre_ids.includes(genreId));

describe("Top Rate Page ", () => {
  before(() => {
    // Get movies from TMDB and store in movies variable.
    cy.request(
      `https://api.themoviedb.org/3/movie/top_rated?api_key=${Cypress.env(
        "TMDB_KEY"
      )}&language=en-US&include_adult=false&include_video=false&page=1`
    )
      .its("body")    // Take the body of HTTP response from TMDB
      .then((response) => {
        movies = response.results
      })
    cy.visit("/");
    cy.get('[data-cy=loginButton]').click();
    cy.get('[data-cy=email]').clear().type(emailString);
    cy.get('[data-cy=password]').clear().type(passwordString);
    cy.get('[data-cy=login]').click();
    cy.url().should("include", `/movies/top_rated`);
  })
  beforeEach(() => {
    cy.visit("/");
    cy.get('[data-cy=toprate_button]').click();
  });


  describe("Base test", () => {
    it("displays page header", () => {
      cy.get("h2").contains("Top Rate Movies");
      cy.get(".badge").contains(20);
    });
  });
  describe("Filtering", () => {
    describe("By movie title", () => {
      it("should display movies with 'part' in the title", () => {
        const searchString = 'part'
        const matchingMovies = filterByTitle(movies, searchString);
        cy.get('[data-cy=listFilter]').type(searchString);
        cy.get('[data-cy=moviecard]').should("have.length", matchingMovies.length);
        cy.get('[data-cy=moviecard]').each(($card, index) => {
          cy.wrap($card)
            .find(".card-title")
            .should("have.text", matchingMovies[index].title);
        });
      })
      it("should display movies with 'go' in the title", () => {
        const searchString = 'go'
        const matchingMovies = filterByTitle(movies, searchString);
        cy.get('[data-cy=listFilter]').type(searchString);
        cy.get('[data-cy=moviecard]').should("have.length", matchingMovies.length);
        cy.get('[data-cy=moviecard]').each(($card, index) => {
          cy.wrap($card)
            .find(".card-title")
            .should("have.text", matchingMovies[index].title);
        });
      })
      it("should display are no matches movies with 'xyz' in the title", () => {
        const searchString = "xyz";
        const matchingMovies = filterByTitle(movies, searchString);
        cy.get('[data-cy=listFilter]').type(searchString);
        cy.get('[data-cy=moviecard]').should("have.length", matchingMovies.length);
        expect(matchingMovies.length).to.equals(0);
        
      })
    })
    describe("By movie genre", () => {
      it("should display movies with the specified genre only", () => {
        const selectedGenreId = 36;
        const matchingMovies = filterByGenre(movies, selectedGenreId);
        cy.get('[data-cy=select]').click();
        cy.get('li[data-value="36"]').click();
        cy.get('[data-cy=moviecard]').should("have.length", matchingMovies.length);
        cy.get('[data-cy=moviecard]').each(($card, index) => {
          cy.wrap($card)
            .find(".card-title")
            .should("have.text", matchingMovies[index].title);
        });
      });
    });
    describe("Both by movie title and genre", () => {
      it("should display movies with the specified text and genre", () => {
        const searchString = "s";
        const matchingMovies = filterByTitle(movies, searchString);
        const selectedGenreId = 36;
        const matchingMovies2 = filterByGenre(matchingMovies, selectedGenreId);
        cy.get('[data-cy=listFilter]').type(searchString);
        cy.get('[data-cy=select]').click();
        cy.get('li[data-value="36"]').click();
        cy.get('[data-cy=moviecard]').should("have.length", matchingMovies2.length);
        cy.get('[data-cy=moviecard]').each(($card, index) => {
          cy.wrap($card)
            .find(".card-title")
            .should("have.text", matchingMovies2[index].title);
        });
      });
    });
  })
  describe("Logout when every test done", () => {
      it("should display login page when logout", () => {
        cy.get('[data-cy=logoutButton]').click();
        cy.url().should("include", `/login`);
      });
    });
})